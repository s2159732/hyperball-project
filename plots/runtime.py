import matplotlib.pyplot as plt

# create data
x = [1995, 2000, 2005, 2010, 2015]

plt.plot(x, [1.66, 6.94, 21.99, 51.58, 120.53], label="Breath first traversal")
plt.plot(x, [2.328, 5.306, 12.858, 22.821, 48.7], label="Hyperball m = 4")
plt.plot(x, [2.95, 5.5, 13.81, 24.2, 46.95], label="Hyperball m = 8")
plt.plot(x, [3.91, 6.21, 15.39, 28.55, 49.17], label="Hyperball m = 16")

plt.xticks(x)
plt.xlabel('Bib graph year')
plt.ylabel('Time (seconds)')
plt.title('Runtime')
plt.legend()
plt.show()

