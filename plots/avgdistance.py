import matplotlib.pyplot as plt


def percentage_away(actual, prediction):
    return prediction


# create data
x = [1995, 2000, 2005, 2010, 2015]

plt.scatter(x, [percentage_away(7.846, 7.844), percentage_away(5.970, 6.547), percentage_away(5.214, 6.188),
             percentage_away(4.873, 5.099), percentage_away(4.709, 4.445)], label="Hyperball m = 4")
plt.scatter(x, [percentage_away(7.846, 7.387), percentage_away(5.970, 5.674), percentage_away(5.214, 5.527),
             percentage_away(4.873, 5.022), percentage_away(4.709, 4.602)], label="Hyperball m = 8")
plt.scatter(x, [percentage_away(7.846, 8.037), percentage_away(5.970, 5.860), percentage_away(5.214, 5.067),
             percentage_away(4.873, 4.689), percentage_away(4.709, 4.450)], label="Hyperball m = 16")
plt.plot(x, [7.846922498337615, 5.970245452894984, 5.214466715581965, 4.873477898644302, 4.709504487681295], label="Breath first traversal")

plt.xticks(x)
plt.xlabel('Bib graph year')
plt.ylabel('Average distance')
plt.title('Average distance per graph')
plt.legend()
plt.show()
