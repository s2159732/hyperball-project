import matplotlib.pyplot as plt


def percentage_away(actual, prediction):
    return abs((prediction / actual) - 1) * 100


# create data
x = [1995, 2000, 2005, 2010, 2015]

plt.scatter(x, [percentage_away(7.846, 7.844), percentage_away(5.970, 6.547), percentage_away(5.214, 6.188),
             percentage_away(4.873, 5.099), percentage_away(4.709, 4.445)], label="Hyperball m = 4")
plt.scatter(x, [percentage_away(7.846, 7.387), percentage_away(5.970, 5.674), percentage_away(5.214, 5.527),
             percentage_away(4.873, 5.022), percentage_away(4.709, 4.602)], label="Hyperball m = 8")
plt.scatter(x, [percentage_away(7.846, 8.037), percentage_away(5.970, 5.860), percentage_away(5.214, 5.067),
             percentage_away(4.873, 4.689), percentage_away(4.709, 4.450)], label="Hyperball m = 16")

plt.xticks(x)
plt.xlabel('Bib graph year')
plt.ylabel('% away from actual avg. distance')
plt.title('Accuracy of Hyperball')
plt.legend()
plt.show()
