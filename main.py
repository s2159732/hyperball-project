import networkx as nx
import scipy as sp
import scipy.io
import utils
from algorithms import breadthfirstsearch, breadthfirsttraversal, hyperball

if __name__ == "__main__":
    print("Loading graph...", end='')
    # Select what graph you want here
    graph = utils.fromDot(open('graphs/bib-graph-2015.dot').read())
    # graph = graphs.F214
    # graph = nx.from_scipy_sparse_array(sp.io.mmread(open('graphs/socfb-Rochester38.mtx', "r+b")))
    print("done")

    # Select the algorithm to use to calculate the average distance between pairs
    hyperball.measureGraph(graph)

