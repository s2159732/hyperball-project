from networkx import Graph
import time

'''
The fast "bfs" algorithm:
Do a breadth first traversal from each node in the graph,
noting down the pairs that we saw along the way.
'''


def measureGraph(graph: Graph):
    sum = 0
    paircount = 0

    print("Computing Avg distance...")

    start_time = time.time()
    node1index = 0
    for start_node in graph.nodes:
        # print a progress indicator
        print(str(node1index) + "/" + str(len(graph.nodes)))

        scanned_nodes = set()
        nodes_to_scan = set()
        nodes_to_scan.add(start_node)

        distance = 1
        while len(nodes_to_scan) != 0:  # loop until it can't find any more nodes
            for node in nodes_to_scan.copy():
                # makes sure we don't scan unnecessary nodes
                for neighbor in set(graph.neighbors(node)) - scanned_nodes - nodes_to_scan:
                    sum += distance
                    paircount += 1
                    nodes_to_scan.add(neighbor)
                scanned_nodes.add(node)
                nodes_to_scan.remove(node)
            distance += 1
        node1index += 1
    print(f"done ({time.time() - start_time} seconds)")

    print("Average distance: " + str(sum / paircount))
