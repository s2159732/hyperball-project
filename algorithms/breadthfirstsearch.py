from networkx import Graph
import time

'''
The slow bfs algorithm:
First find all pairs in the graph, then do a breadth first search 
to find a path between them.

'''

def bfs_get_distance(graph: Graph, start, end):
    scanned_nodes = set()
    nodes_to_scan = set()
    nodes_to_scan.add(start)

    distance = 1

    while True:
        if len(nodes_to_scan) == 0:
            return 0  # unreachable

        for node in nodes_to_scan.copy():
            # makes sure we don't scan unnecessary nodes
            for neighbor in set(graph.neighbors(node)) - scanned_nodes - nodes_to_scan:
                if neighbor == end:
                    return distance
                elif neighbor not in scanned_nodes:
                    nodes_to_scan.add(neighbor)
            scanned_nodes.add(node)
            nodes_to_scan.remove(node)
        distance += 1


def measureGraph(graph: Graph):
    sum = 0
    paircount = 0

    print("Computing Avg distance...")

    start_time = time.time()
    node1index = 0
    # first find all pairs (no X-X pairs and no reversed X-Y/Y-X)
    for node1 in graph.nodes:
        print(str(node1index) + "/" + str(len(graph.nodes)))
        node2index = 0
        for node2 in graph.nodes:
            if node1 != node2 and node2index > node1index:
                paircount += 1

                # get the distance for each of the pairs
                distance = bfs_get_distance(graph, node1, node2)
                if distance == 0:
                    paircount -= 1  # unreachable, not considered a pair anymore
                else:
                    sum += distance
            node2index += 1
        node1index += 1
    print(f"done ({time.time() - start_time} seconds)")

    print("Average distance: " + str(sum / paircount))
