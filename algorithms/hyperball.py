import numpy as np
import hashlib
import time
import matplotlib.pyplot as plt
from networkx import Graph

'''
The implementation of the Hyperball algorithm
from the paper of Paolo Boldi et al.
'''

b = 3
m = np.power(2, b)
alpha = 0.7213/(1 + 1.079/m)
prefix = "distanceData/distancesAt"

def addHLL(M, x):
    '''
    Adds an item to an HLL counter
    :param M: The counter to which the item is added
    :param x: The item to be added to the counter
    '''
    #Calculates hash of the item
    h = bin(int(hashlib.md5(str(x).encode()).hexdigest(), 16))
    #Finds the index of the register it will be added to
    i = int(h[4:b+4], 2)
    #Counts the amount of leading zeros
    leading0 = 0
    while h[b+4+leading0] == '0':
        leading0 += 1
    leading0 += 1
    #Adds it to the counter
    M[i] = max(M[i], leading0)

def sizeHLL(M):
    '''
    Calculates the size of counter M
    :param M: The counter to calculate the size for
    :returns: The size of counter M
    '''
    #Calculates the harmonic average of the values in the register
    Z = 0
    for r in M:
        Z += 1 / np.power(2, r)
    #Calculates the size of the counter
    Z = np.power(Z, (-1))
    E = np.square(m) * Z * alpha
    return E

def unionHB(M, N):
    '''
    Returns the union of counter M and N
    :param N: An HLL counter
    :param M: Another HLL counter
    :returns: The union of M and N
    '''
    res = np.zeros(m)
    for ii in range(m):
        res[ii] = max(M[ii], N[ii])
    return res

def hyperBall(G):
    '''
    Uses the HyperBall algorithm to calculate the distance distribution in graph G
    :param G: The graph of which you want to know the distance distribution
    :returns: The distance distribution of graph G
    '''
    n = list(G.nodes())
    c = np.zeros((len(n), m)) #Optimize with hashmap
    c1 = np.zeros((len(n), m))

    #Adds every node to it's own counter
    for ii in range(len(n)):
        addHLL(c[ii], n[ii])

    t = 0
    stop = False

    while not stop:
        #Takes the union of every nodes counter with the counter of all it's neighbours
        for ii in range(len(n)):
            v = n[ii]
            a = c[ii].copy()
            for w in G.neighbors(v):
                wIndex = findIndex(w, n)
                a = unionHB(a, c[wIndex])
                c1[ii] = a.copy()
        #save result
        filename = prefix + str(t) + ".csv"
        np.savetxt(filename, c1, delimiter=",", fmt="%d")
        #Checks whether all counters have stayed the same this iteration
        if np.array_equal(c,c1):
            stop = True
        else:
            c = c1.copy()
            t += 1

    #Calculates the distances
    dist = calcBall(len(n), t)
    return dist
    #plotDistanceBar(dist/np.sum(dist))

def calcBall(lenn , tmax):
    '''
    Calculates the distances of the node pairs in a graph
    :param lenn: The amount of nodes in the graph
    :param tmax: The maximum distance between two nodes
    :returns: an array with for every distance the amount of nodes that have that distance between them
    '''
    distances = np.zeros(tmax + 1)
    nodesPrevDistance = np.ones(lenn)
    for t in range(tmax + 1):
        #open csv files with the HLL counters
        filename = prefix + str(t) + ".csv"
        f = open(filename)
        ii = 0
        for v in f:
            #format data
            v = v.split(",")
            v = [int(s) for s in v]
            #Calculate estimation of the ball size
            sizet = sizeHLL(v)
            distances[t] += sizet - nodesPrevDistance[ii]
            nodesPrevDistance[ii] = sizet
            ii += 1
    return distances


def avgDist(distances):
    '''
    Calculates the average distance given a array of distances
    :param distances: The array with distances
    :returns: The average distance
    '''
    wsum = 0
    sum = 0
    #Calculates the weighted average of the distance
    for ii in range(len(distances)):
        wsum += (ii + 1) * distances[ii]
        sum += distances[ii]
    return wsum / sum



def findIndex(w, N):
    '''
    Finds the index of an item in an array
    :param w: The item you want to know the index of
    :param N: The array with the item in it
    :returns: The index of the item
    '''
    for ii in range(len(N)):
        if w == N[ii]:
            return ii

def plotDistanceBar(distances):
    '''
    Creates a plot of the given distance distribution
    :param distances: the distance distribution
    '''
    plt.bar(range(1, len(distances) + 1), distances)
    plt.title("Fraction of node pairs per distance - 2015")
    plt.xlabel("Distance")
    plt.ylabel("Fraction of node pairs")
    plt.show()

def measureGraph(graph: Graph):
    '''
    Calculates the average distance of a graph
    :param Graph: The graph you want to know the average distance of
    '''
    start = time.time()
    dist = hyperBall(graph)
    end = time.time()
    print("Average distance is " + str(avgDist(dist)))
    print("graph processed in " + str(round(end-start, 3)) + "s")